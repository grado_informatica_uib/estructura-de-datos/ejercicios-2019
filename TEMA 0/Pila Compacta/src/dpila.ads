-- ESTRUCTURA DE DATOS DE UNA PILA.
generic
   type elem is private;
   max: Natural:=100;
package DPILA is
   type pila is limited private;
   mal_uso:Exception;
   memoria_desbordada:Exception;

   procedure pvacia(p: out pila);
   procedure empilar(p: in out pila; x: elem);
   procedure desempilar(p: in out pila);
   function cima(p: in pila) return elem;
   function esta_vacia(p: in pila) return boolean;

private
   type indice is new Integer range 0..max;
   type espacio_memoria is array(indice range 1..indice'last) of elem;
   type pila is record
      a: espacio_memoria;
      i: indice;
   end record;
end DPILA;
