with DPILA;
with ada.Text_IO;
use  ada.Text_IO;
procedure Main is
   package mipila is new DPILA(Character);
   use mipila;
   p: pila;
   s : String := "hola";
begin
   Put_Line("-------------------------------------------------------");
   Put_Line("                      PROGRAMA ADA                     ");
   Put_Line("-------------------------------------------------------");
   Put_Line("Programa que muestra una palabra invertida.");

   Put_Line("La palabra inicial es: " & s);
   -- Procedemos a empilar los datos en la pila.
   Put_Line("Vamos empilando: ");
   for i in 1..s'Length loop
      empilar(p,s(i));
      Put(Character'Image(cima(p)));
   end loop;
   Put_Line("");
   -- Desempilamos los datos y los imprimimos.
   Put_Line("Palabra invertida: ");
   for i in 1..s'Length loop
      Put(Character'Image(cima(p)));
      desempilar(p);
   end loop;

end Main;
