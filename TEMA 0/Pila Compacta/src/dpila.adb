with ada.Text_IO;
use  ada.Text_IO;
package body DPILA is
   procedure pvacia(p: out pila) is
     i: indice renames p.i;
   begin
      i:=0;
      Put_Line("Pila a cero");
   end pvacia;

   procedure empilar(p: in out pila; x: elem) is
   begin
      p.i:=p.i+1;
      p.a(p.i):=x;
   exception
      when constraint_error => raise memoria_desbordada;
   end empilar;

   procedure desempilar(p: in out pila) is
   begin
      p.i:= p.i -1;
   exception
      when constraint_error => raise mal_uso;
   end desempilar;

   function cima(p: in pila) return elem is
   begin
      return p.a(p.i);
   exception
      when constraint_error => raise mal_uso;
   end cima;

   function esta_vacia(p: in pila) return boolean is
   begin
      if p.i = 0 then
         return true;
      end if;
      return false;
   end esta_vacia;
end DPILA;
